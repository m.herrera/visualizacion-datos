# primero instalamos librerias con pip - pandas, numpy, seaborn, matplotlib

# librerias
import pandas as pd
pd.plotting.register_matplotlib_converters()
import matplotlib.pyplot as plt
#%matplotlib inline
import seaborn as sns
print("Setup Complete")

# ruta del archivo
fifa_filepath = "fifa.csv"

# leer el archivo en una variable
fifa_data = pd.read_csv(fifa_filepath, index_col="Date", parse_dates=True)

# examinar datos
# mostramos los primeros 5 registros(filas)
fifa_data.head()

# graficando los datos
# asignamos ancho y alto del grafico
plt.figure(figsize=(16,6))

# mostramos el dataset
sns.lineplot(data=fifa_data)
plt.show()